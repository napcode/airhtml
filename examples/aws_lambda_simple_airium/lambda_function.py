"""
Follow README.md for setup instructions.
"""

from datetime import datetime

from awslambdaric.lambda_context import LambdaContext

from airium import Airium


def lambda_handler(event: dict, context: LambdaContext) -> dict:
    a = Airium(base_indent="", source_minify=True)

    time_now = datetime.now().strftime("%Y/%m/%d, %H:%M:%S")

    a("<!DOCTYPE html>")
    with a.html(lang="pl"):
        with a.head():
            a.meta(charset="utf-8")
            a.title(_t="Airium Lambda Example")

        with a.body():
            with a.h1(id="id23409231", klass="main_header"):
                a("Airium - Hello World")
            a(f"It's {time_now} right now (UTC).")

            with a.div():
                a.h3(_t=f"event object [{type(event).__name__}]")
                with a.ul():
                    for k, v in event.items():
                        if isinstance(v, dict):
                            with a.li().ul():
                                for ks, vs in event.items():
                                    a.li(_t=f"{ks!r} [{type(vs).__name__}] = {vs!r}")
                        else:
                            a.li(_t=f"{k!r} [{type(v).__name__}] = {v!r}")

            with a.div():
                a.h3(_t=f"context object [{type(context).__name__}]")
                with a.ul():
                    for k in dir(context):
                        if not k.startswith("_"):
                            v = getattr(context, k, None)
                            a.li(_t=f"{k!r} [{type(k).__name__}] = {v!r}")

    return {
        "statusCode": 200,
        "body": bytes(a),
        "headers": {"content-type": "text/html"},
    }
