function add_watch_to(src_lang, dst_lang) {
    const src_text_area = document.getElementById(`${src_lang}-code-text`)

    src_text_area.onkeyup = function do_translate() {
        function handle_response() {
            if (this.readyState === 4) { // readyState == DONE - The operation is complete.
                const dest_text = document.getElementById(`${dst_lang}-code-text`);
                if (xhttp.status === 200) {
                    const payload = JSON.parse(this.response);
                    dest_text.value = payload.content;
                    dest_text.style.color = (payload.is_error ? "red" : "black")
                    document.getElementById(`${dst_lang}-code-time`).innerText = payload.time_consumed;
                    if (!payload.is_error) {
                        document.getElementById("preview-frame").contentWindow.location.reload();
                    }
                } else {
                    console.log(`Not OK ${this.responseText}`)
                    dest_text.value = this.responseText;
                }
            }
        }

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = handle_response;
        // xhttp.responseType = 'json'
        xhttp.open("POST", "/translate", true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify({
            language: src_lang,
            content: src_text_area.value
        }));
        document.getElementById(`${src_lang}-code-time`).innerText = "-";
    };
}

add_watch_to("py", "html")
add_watch_to("html", "py")
