import os

from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from starlette.staticfiles import StaticFiles

from polyglot.schemas import Context, TranslationRequest, TranslationResponse
from polyglot.views import index_page


this_dir_path = os.path.dirname(os.path.abspath(__file__))
static_dir_path = os.path.join(this_dir_path, 'static')

app = FastAPI()
app.mount("/static", StaticFiles(directory="polyglot/static"), name="static")


@app.get("/", response_class=HTMLResponse)
async def web_root():
    return await index_page()


@app.get("/last-translation", response_class=HTMLResponse)
async def last_translation():
    return Context.last_translation


@app.post("/translate", response_model=TranslationResponse)
async def post_translation_document(document: TranslationRequest):
    return TranslationResponse.from_translation_request(document)
