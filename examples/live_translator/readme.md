# airium-polyglot - usage example

This is an example of using airium with:
 - fast api
 - java script with legacy XMLHttpRequest
 - docker

It serves a bidirectional translation of airium and HTML code. 

![airium-polyglot screenshot](readme_main.png)

The code of this example is ready to be run just by calling simple docker-compose commands.
First checkout this repository and then call

```commandline
cd AIRIUM_SRC_DIR_PATH/live_translator
docker-compose build
docker-compose up
```

Then visit just [http://0.0.0.0:8123/](http://0.0.0.0:8123/) with your web browser.
