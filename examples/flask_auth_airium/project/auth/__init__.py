"""
Basing on tutorial created by Anthony Herbert:
https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login
https://github.com/PrettyPrinted/flask_auth_scotch

Airium usage added by Michał Kaczmarczyk
"""

from flask import Blueprint, flash, redirect, request, url_for
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.security import check_password_hash, generate_password_hash

from .views import login_block, signup_block
from .. import db
from ..auth.models import User
from ..main.views import base_wrapper

auth = Blueprint("auth", __name__)


@auth.route("/login")
def login():
    if current_user.is_authenticated:
        return redirect(url_for("main.profile"))

    with base_wrapper() as a:
        login_block(a)
        return str(a)


@auth.route("/login", methods=["POST"])
def login_post():
    email = request.form.get("email")
    password = request.form.get("password")
    remember = True if request.form.get("remember") else False

    user = User.query.filter_by(email=email).first()

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not check_password_hash(user.password, password):
        flash("Please check your login details and try again.")
        return redirect(
            url_for("auth.login")
        )  # if user doesn't exist or password is wrong, reload the page

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for("main.profile"))


@auth.route("/signup")
def signup():
    if current_user.is_authenticated:
        logout_user()
        return redirect(url_for("auth.signup"))

    with base_wrapper() as a:
        signup_block(a)
        return str(a)


@auth.route("/signup", methods=["POST"])
def signup_post():
    email = request.form.get("email")
    name = request.form.get("name")
    password = request.form.get("password")

    user = User.query.filter_by(email=email).first()
    # if this returns a user, then the email already exists in database

    if user:
        # if a user is found, we want to redirect back to signup page so user can try again
        flash("Email address already exists. ")
        return redirect(url_for("auth.signup"))

    # create new user with the form data. Hash the password so plaintext version isn't saved.
    new_user = User(
        email=email, name=name, password=generate_password_hash(password, method="sha256")
    )

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for("auth.login"))


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main.index"))
